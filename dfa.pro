%   dfa - a Deterministic Finite Automaton verificator in Prolog
%
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%   Copyright (C) 2016  Filipe Saraiva <mail@filipesaraiva.info>
%

dfa_def(Sym, Sta, Prod, Ini, Fin) :-
        def_sym(Sym),
        def_states(Sta),
        def_prod(Prod),
        def_ini(Ini),
        def_fin(Fin).

dfa_parse(W) :-
        initial(X),
        atom_chars(W, C),
        parse(C, X), !.

parse([], S) :- final(S).
parse([X|Y], S) :-
        prod(S, N, X),
        parse(Y, N).

def_sym([]).
def_sym([X|Y]) :-
        assertz(symbol(X)),
        def_sym(Y).

def_states([]).
def_states([X|Y]) :-
        assertz(state(X)),
        def_states(Y).

def_prod([]).
def_prod([X|Y]) :-
        term_to_atom(X,A),
        split_string(A, ",", "()", L),
        nth0(0, L, Ci),
        nth0(1, L, Cf),
        nth0(2, L, Cc),
        atom_string(Qi, Ci),
        atom_string(Qf, Cf),
        atom_string(C, Cc),
        assertz(prod(Qi, Qf, C)),
        def_prod(Y).

def_ini(X) :-
        assertz(initial(X)).

def_fin([]).
def_fin([X|Y]) :-
        assertz(final(X)),
        def_fin(Y).
